# No Camera Visuals

This repository holds the visuals for the [No Camera project](https://gitlab.com/lurnid/no-camera).

Unless otherwise stated, the works in this repository are the copyright of their respective authors and contributors, and licenced under a [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) license.
